﻿using System;
using System.Windows.Forms;

namespace dmMigrationTool
{
    public partial class PasswordForm : Form
    {
        public string password { get; set; }
        
        public PasswordForm()
        {
            InitializeComponent();
            password = "";
            this.AcceptButton = btnLogin;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            this.password = txtLoginPassword.Text;
            this.Close();
        }
    }
}
