﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Security.Principal;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace dmMigrationTool
{
    public partial class Migration_Tool : Form
    {
        private readonly string LOGIN_PASSWORD = "Autoform1";
        private readonly string DATE_FORMAT = @"dd/MM/yyyy HH:mm:ss";
        private readonly string NO_LOG_KEYWORD = "NOLOG";
        private readonly string NOUPDATEKEY = "No Update";

        private readonly Validation validate;
        private readonly Sql sql;
        private readonly Logging logging;
        private readonly StatusTracker statusTracker;

        private string destination, archive, javaLocation, compressed;
        private int totalRecords = 0;
        private int count = 0;

        public Migration_Tool()
        {
            if (!CheckPassword(NO_LOG_KEYWORD))
                Environment.Exit(1);
            
            InitializeComponent();

            if(new WindowsPrincipal(WindowsIdentity.GetCurrent()).IsInRole(WindowsBuiltInRole.Administrator))
                lblAdmin.Visible = true;
            
            validate = new Validation();
            validate.SetDFMT(false, "");
            sql = new Sql();
            logging = new Logging(this);
            statusTracker = new StatusTracker();

            var keyLists = new KeyList(NOUPDATEKEY);
            cmbNewKey.Items.AddRange(keyLists.pdmKeys.ToArray());
            cmbValue.Items.AddRange(keyLists.sqlFields.ToArray());
            cmbValue.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cmbUpdateKey.Items.AddRange(keyLists.updateKeys.ToArray());
            cmbUpdateKey.SelectedIndex = 0;

            lstKeys.Columns.Add("Keys");
            lstKeys.Columns.Add("Value");
            lstKeys.View = View.Details;

            this.Refresh();
        }

        private string FixPathFormat(string path)
        {
            if(!path.EndsWith("\\"))
                path += '\\';
            return path;
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            if (!CheckPassword("running"))
                return;

            logging.WriteLog("Started running");
            lblCounter.Text = "";
            logging.WriteLog("Checking Folders Locations");
            this.Refresh();
            destination = FixPathFormat(txtDestination.Text);
            compressed = FixPathFormat(txtCompressed.Text);
            archive = FixPathFormat(txtArchive.Text);
            logging.WriteLog("---> Archive Path = " + archive);
            javaLocation = txtJava.Text;
            if (!(validate.Folder(destination, false)&& validate.Folder(compressed, false) && validate.Folder(archive, false) && validate.Folder(javaLocation, true)))
            {
                MessageBox.Show("There was an error with the destination folders", "Folder Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                logging.WriteLog("ERROR", "Error when checking the folders");
                this.Refresh();
                return;
            }

            if (!Directory.Exists(compressed))
                Directory.CreateDirectory(compressed);
            logging.WriteLog("Folders are correct, checking SQL");
            this.Refresh();

            sql.SetDb(txtServer.Text, txtDatabase.Text, txtUsername.Text, txtPassword.Text);

            var connectionString = "Server=" + sql.dbUrl + ";Database=" + sql.dbDatabase + ";User Id=" + sql.dbUser + ";Password=" + sql.dbPassword + ";";
            logging.WriteLog("SQL connection string = " + connectionString);
            SqlConnection myConnection = new SqlConnection(connectionString);
            try
            {
                myConnection.Open();
            }
            catch(SqlException exception)
            {
                MessageBox.Show("There was an error with the sql", "SQL Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                logging.WriteLog("ERROR", "There was an error trying the SQL connection\n" + exception.ToString());
                this.Refresh();
                return;
            }
            logging.WriteLog("Connected to SQL, will now run the query");
            SearchSQL(myConnection, connectionString, sql.CreateSelectQuery(false, txtMaxRecords.Text, txtTable.Text, txtWhere.Text), false);
            myConnection.Close();

            logging.WriteLog("DEBUG", "Finsihed running");
            ResetCountersShowReport();
            this.Refresh();
        }

        private void ResetCountersShowReport()
        {
            string report = statusTracker.GenerateReport();
            logging.WriteLog("INFO", report);
            count = 0;
            totalRecords = 0;
        }

        private void SearchSQL(SqlConnection myConnection, string connectionString, string query, bool count)
        {
            var myCommand = new SqlCommand(query, myConnection);
            logging.WriteLog("Query run := " + query);
            SqlDataReader sqlData = myCommand.ExecuteReader();
            logging.WriteLog("Reading data");

            SqlConnection updateConnection = CreateUpdateConnection(connectionString);
            if (updateConnection == null)
            {
                logging.WriteLog("ERROR", "Records cannot be updated");
                Environment.Exit(1);
            }

            while(sqlData.Read())
            {
                if (count)
                {
                    var total = (int)sqlData[0];
                    var maxRcordCount = 0;
                    if (int.TryParse(txtMaxRecords.Text.ToString(), out maxRcordCount) && maxRcordCount <= total)
                    {
                        lblCounter.Text = "Document 0/" + maxRcordCount;
                        total = maxRcordCount;
                    }
                    else
                        lblCounter.Text = "Document 0/" + total;
                    this.Refresh();
                    logging.WriteLog("Count completed found a total of " + total + " records");
                    totalRecords = total;
                }
                else
                {
                    logging.WriteLog("Records found will now start to copy the files");
                    CopyToCompressed(updateConnection, sqlData, connectionString);
                }
            }
            updateConnection.Dispose();
            updateConnection.Close();
        }

        private SqlConnection CreateUpdateConnection(string connectionString)
        {
            SqlConnection updateConnection;
            try
            {
                updateConnection = new SqlConnection(connectionString);
                logging.WriteLog("Creating connection to the database");
                updateConnection.Open();
            }
            catch (Exception exception)
            {
                logging.WriteLog("ERROR", "There was an issue connecting to the database\n" + exception);
                return null;
            }
            return updateConnection;
        }

        private void CopyToCompressed(SqlConnection updateConnection, SqlDataReader sqlData, string connectionString)
        {
            var compressedoutput = "";
            var path = "";
            if (sqlData[7].ToString() == "")
            {
                logging.WriteLog("No file has been archived agasint document " + sqlData[0]);
                compressedoutput = compressed + sqlData[0] + ".pdf";
            }
            else
            {
                var archiveLocation = sqlData[7].ToString();
                logging.WriteLog("Archive location " + archiveLocation);
                var dateFolder = "";
                try
                {
                    dateFolder = DateTime.ParseExact(archiveLocation, DATE_FORMAT, System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy$$MM$$dd$$HH");
                }
                catch
                {
                    string[] archiveSplit = archiveLocation.Split('-');
                    dateFolder = archiveLocation[0] + "\\" + archiveLocation[1] + "\\" + archiveLocation[2] + "\\" + archiveLocation[0].ToString().Split(':')[0];
                }
                path = archive + dateFolder;
                path = path.Replace("$$", "\\");
                path += "\\" + sqlData[0].ToString() + "_" + sqlData[3].ToString() + "." + sqlData[14].ToString();
                logging.WriteLog("File (" + sqlData[0] + ") is archived and checking location " + path);
                this.Refresh();
                compressedoutput = compressed + Path.GetFileName(path);
                logging.WriteLog("Looking to copy file to " + compressedoutput);
            }
            
            if (File.Exists(path) && !File.Exists(compressedoutput))
            {
                File.Copy(path, compressedoutput);
                logging.WriteLog("File copied, will now decompress");
                UpdateDatabaseRecord(updateConnection, connectionString, sqlData[0].ToString());
                DecompressArchive(sqlData);
            }
            else if (path == null)
            {
                File.WriteAllText(compressedoutput, "");
                logging.WriteLog("New file created (no match in archive) will now write the data to the file");
                UpdateDatabaseRecord(updateConnection, connectionString, sqlData[0].ToString());
                WriteNewFiles(sqlData);
            }
            else
            {
                logging.WriteLog("WARN", "There was an issue with the file, will move onto next one");
                statusTracker.ProcessedDoc(sqlData[0].ToString());
                return;
            }
        }

        private void UpdateDatabaseRecord(SqlConnection updateConnection, string connectionString, string docID)
        {
            if (updateConnection == null)
                return;
            logging.WriteLog("Updating record with docID " + docID);
            if (cmbUpdateKey.Text != NOUPDATEKEY && txtUpdateValue.Text != "")
            {               
                var query = "UPDATE tblDocuments SET " + cmbUpdateKey.Text + "='" + txtUpdateValue.Text + "' WHERE docID = " + docID + ";";
                logging.WriteLog("Running update command\n" + query);

                var sqlCommand = new SqlCommand(query, updateConnection);
                try 
                {
                    sqlCommand.ExecuteNonQuery();
                    sqlCommand.Dispose();
                }
                catch (Exception exception)
                {
                    logging.WriteLog("ERROR", "Error when trying to update database\n" + exception);
                    return;
                }
                
                logging.WriteLog("Record updated");
            }
            else
            {
                logging.WriteLog("Update value is blank, will not update the record");
            }
        }

        private void DecompressArchive(SqlDataReader sqlData)
        {
            logging.WriteLog("Decompressing Document " + sqlData[0]);

            var workingDirectory = System.Reflection.Assembly.GetEntryAssembly().Location.ToString();
            workingDirectory = Directory.GetParent(workingDirectory).ToString();

            var arguments = " DocumentExtractor \"" + compressed.Substring(0, compressed.Length-1) + "\"";
            var commandLine = "\"" + javaLocation + "\" -classpath .;commons-io-1.4.jar" + arguments;

            logging.WriteLog("CMD - " + commandLine);

            var process = new Process();
            var startInfo = new ProcessStartInfo();
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.CreateNoWindow = true;
            
            startInfo.WorkingDirectory = workingDirectory;
            logging.WriteLog("CMD Dir - " + startInfo.WorkingDirectory);

            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardInput = true;

            startInfo.FileName = "cmd.exe";
            process.StartInfo = startInfo;
            process.Start();

            process.StandardInput.WriteLine(@"cd " + workingDirectory);
            process.StandardInput.WriteLine(commandLine);
            process.StandardInput.WriteLine("exit");

            var commandLineOut = process.StandardOutput.ReadToEnd();
            logging.WriteLog("CMD Out - " + commandLineOut);

            process.WaitForExit();
            process.Close();
            logging.WriteLog("Document has been decompressed");
            WriteNewFiles(sqlData);
        }

        private void WriteNewFiles(SqlDataReader sqlData)
        {
            string[] files = Directory.GetFiles(compressed);
            foreach (string uncompressedFile in files)
            {
                count++;
                lblCounter.Text = "Document " + count + "/" + totalRecords;
                logging.WriteLog("Working on document " + count + " out of " + totalRecords);
                this.Refresh();
                List<string> keys = new List<string>();

                logging.WriteLog("Finding keys and their values");

                foreach (ListViewItem item in lstKeys.Items)
                {
                    var key = item.SubItems[0].Text;
                    var data = item.SubItems[1].Text;
                    string[] dataSplit = data.Split('+');
                    data = "";
                    foreach (string addtion in dataSplit)
                    {
                        data += CheckSQL(addtion, sqlData);
                    }
                    logging.WriteLog("Adding key " + key + " with value " + data);
                    keys.Add("#" + key + " " + data + "#");
                }

                var outFileName = txtOutName.Text;
                var keyRegex = "#.+?#";
                var keysInName = Regex.Matches(outFileName, keyRegex);
                var keyList = new KeyList();
                bool increment = false;
                foreach (var match in keysInName)
                {
                    logging.WriteLog("Match in file name => '" + match.ToString() + "'");
                    var databaseMap = keyList.GetDatabaseMap();
                    var databaseRow = databaseMap.IndexOf(match.ToString().Replace("#", ""));
                    if (databaseRow != -1)
                        outFileName = outFileName.Replace(match.ToString(), sqlData[databaseRow].ToString());
                    else if (match.ToString() == "#i#")
                        increment = true;
                }

                if (increment)
                {
                    for(int i = 1; i < 10; i++)
                    {
                        if(!File.Exists(outFileName.Replace("#i#", i.ToString())))
                        {
                            outFileName = outFileName.Replace("#i#", i.ToString());
                            break;
                        }
                    }
                }
        
                logging.WriteLog("Out file name => '" + outFileName + "'");

                var output = destination + Path.GetFileName(uncompressedFile).Replace("U_", "");
                if (sqlData[7].ToString() == "")
                {
                    logging.WriteLog("Setting PDMATT to no as file should not be archived");
                    keys.Add("#PDMATT no#");
                    output = destination + sqlData[0] + "." + sqlData[14];
                }
                logging.WriteLog("Looking to write new file at " + output);
                try
                {
                    File.WriteAllLines(output, keys.ToArray());
                    byte[] file = File.ReadAllBytes(uncompressedFile);
                    using (var stream = new FileStream(output, FileMode.Append))
                    {
                        stream.Write(file, 0, file.Length);
                    }
                    logging.WriteLog("File has been written will now remove the old file");
                    statusTracker.ProcessedDoc();
                }
                catch(Exception exception)
                {
                    logging.WriteLog("ERROR", "There was an issue when writing file " + sqlData[0].ToString() + "\n" + exception);
                    statusTracker.ProcessedDoc(sqlData[0].ToString());
                }

                try { File.Delete(uncompressedFile); }
                catch (Exception exception) { logging.WriteLog("ERROR", "There was an issue when deleting file " + uncompressedFile + "\n" + exception); }
                
                logging.WriteLog("Old file has now been removed, moving onto the next file");
            }
        }

        private string CheckSQL(string data, SqlDataReader sqlData)
        {
            if (cmbValue.Items.Contains(data))
                return sqlData[cmbValue.Items.IndexOf(data)].ToString();
            return data;
        }

        private void txtMaxRecords_TextChanged(object sender, EventArgs e)
        {
            rtxtQueryOutput.Text = sql.CreateSelectQuery(false, txtMaxRecords.Text, txtTable.Text, txtWhere.Text);
        }

        private void txtTable_TextChanged(object sender, EventArgs e)
        {
            rtxtQueryOutput.Text = sql.CreateSelectQuery(false, txtMaxRecords.Text, txtTable.Text, txtWhere.Text);
        }

        private void txtWhere_TextChanged(object sender, EventArgs e)
        {
            rtxtQueryOutput.Text = sql.CreateSelectQuery(false, txtMaxRecords.Text, txtTable.Text, txtWhere.Text);
        }

        private void btnAddKey_Click(object sender, EventArgs e)
        {
            string key = cmbNewKey.Text;
            string data = cmbValue.Text;
            if (key != "" && data != "")
            {
                var keyList = new KeyList("temp");
                var keyArray = new string[2] { key, data };
                if (key.Contains("PDMDFMT"))
                    validate.SetDFMT(true, data);

                if(validate.CheckKey(key, data))
                {
                    lstKeys.Items.Add(new ListViewItem(keyArray));
                    cmbNewKey.Items.Remove(key);
                    cmbNewKey.SelectedIndex = 0;
                    cmbValue.Text = "";
                }
            }
        }

        private void btnRemoveKey_Click(object sender, EventArgs e)
        {
            if (!CheckPassword("removing key"))
                return;

            foreach(ListViewItem item in lstKeys.SelectedItems)
            {
                lstKeys.Items.Remove(item);
                var keyLists = new KeyList("temp");
                cmbNewKey.Items.Clear();
                cmbNewKey.Items.AddRange(keyLists.pdmKeys.ToArray());
                foreach (ListViewItem allItems in lstKeys.Items)
                {
                    cmbNewKey.Items.Remove(allItems.Text);
                    if (allItems.Text.Contains("PDMDFMT"))
                        validate.SetDFMT(false, "");
                }
            }
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            foreach(ListViewItem item in lstKeys.SelectedItems)
            {
                var index = item.Index;
                if (index > 0)
                {
                    lstKeys.Items.Remove(item);
                    lstKeys.Items.Insert(index - 1, item);
                }
            }
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lstKeys.SelectedItems)
            {
                var index = item.Index;
                if (index < lstKeys.Items.Count-1)
                {
                    lstKeys.Items.Remove(item);
                    lstKeys.Items.Insert(index + 1, item);
                }
            }
        }

        private void btnCount_Click(object sender, EventArgs e)
        {
            if (!CheckPassword("running count"))
                return;

            var dbUrl = txtServer.Text;
            var dbDatabase = txtDatabase.Text;
            var dbUser = txtUsername.Text;
            var dbPassword = txtPassword.Text;

            var connectionString = "Server=" + dbUrl + ";Database=" + dbDatabase + ";User Id=" + dbUser + ";Password=" + dbPassword + ";";
            var myConnection = new SqlConnection(connectionString);
            try
            {
                myConnection.Open();
            }
            catch (SqlException exception)
            {
                MessageBox.Show("There was an error with the sql", "SQL Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                logging.WriteLog("Error when connecting to the database\n", exception.ToString());
                btnRun.Enabled = false;
                this.Refresh();
                return;
            }
            SearchSQL(myConnection, connectionString, sql.CreateSelectQuery(true, txtMaxRecords.Text, txtTable.Text, txtWhere.Text), true);
            myConnection.Close();
            btnRun.Enabled = true;
        }

        private void btnDestFolder_Click(object sender, EventArgs e)
        {
            if (fbdFindFolder.ShowDialog() == DialogResult.OK)
                txtDestination.Text = fbdFindFolder.SelectedPath + '\\';
        }

        private void btnCompFolder_Click(object sender, EventArgs e)
        {
            if (fbdFindFolder.ShowDialog() == DialogResult.OK)
                txtCompressed.Text = fbdFindFolder.SelectedPath + '\\';
        }

        private void btnArchiveFolder_Click(object sender, EventArgs e)
        {
            if (fbdFindFolder.ShowDialog() == DialogResult.OK)
                txtArchive.Text = fbdFindFolder.SelectedPath + '\\';
        }

        private void btnJavaLocation_Click(object sender, EventArgs e)
        {
            if (ofdJavaFile.ShowDialog() == DialogResult.OK)
                txtJava.Text = ofdJavaFile.FileName;
        }

        private void btnExportConfig_Click(object sender, EventArgs e)
        {
            if (sfdSaveFile.ShowDialog() == DialogResult.OK)
            {
                var saveConfig = new SaveConfig(this, sfdSaveFile.FileName);
                saveConfig.SaveValues();
                logging.WriteLog("DEBUG", "Config export complete. " + sfdSaveFile.FileName);
            }
        }

        private void btnImportConfig_Click(object sender, EventArgs e)
        {
            if(CheckPassword("importing config") && ofdConfigFile.ShowDialog() == DialogResult.OK)
            {
                var importConfig = new ImportConfig(this, ofdConfigFile.FileName);
                importConfig.ImportValues();
                logging.WriteLog("DEBUG", "Import Complete " + ofdConfigFile.FileName);
            }
        }

        private bool CheckPassword(string usage)
        {
           /* var passwordForm = new PasswordForm();
            passwordForm.ShowDialog();
            if (passwordForm.password == LOGIN_PASSWORD)
            {
                if (usage != NO_LOG_KEYWORD)
                    logging.WriteLog("INFO", "Correct password entered for " + usage);
            }
            else
            {
                MessageBox.Show("Password Incorrect", "Password Incorrect", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //logging.WriteLog("INFO", "Incorrect password entered for " + usage);
                return false;
            }*/
            return true;
        }

        private void btnOutNameInfo_Click(object sender, EventArgs e)
        {
            FileNameInformation fileNameInfo = new FileNameInformation();
            fileNameInfo.ShowDialog();
        }

    }
}
