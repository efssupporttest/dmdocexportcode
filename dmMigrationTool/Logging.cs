﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace dmMigrationTool
{
    class Logging
    {
        private readonly ILog _logger = log4net.LogManager.GetLogger(typeof(Program));
        private readonly Migration_Tool migrationForm;
        private readonly LoggingWindow loggingWindow;

        public Logging(Migration_Tool migrationtool)
        {
            migrationForm = migrationtool;
            loggingWindow = new LoggingWindow();
            loggingWindow.Show();
        }

        public void WriteLog(string level, string message)
        {
            loggingWindow.rtxtLogWindow.AppendText("\n[" + DateTime.Now.ToString("HH:mm:ss dd-MM-yy") + "] " + level + " - " + message);
            loggingWindow.rtxtLogWindow.ScrollToCaret();
            switch(level)
            {
                case "DEBUG":
                    _logger.Debug(message);
                    break;
                case "INFO":
                    _logger.Info(message);
                    break;
                case "WARN":
                    _logger.Warn(message);
                    break;
                case "ERROR":
                    _logger.Error(message);
                    break; 
            }
        }

        public void WriteLog(string message)
        {
            loggingWindow.rtxtLogWindow.AppendText("\n[" + DateTime.Now.ToString("HH:mm:ss dd-MM-yy") + "] DEBUG" + " - " + message);
            loggingWindow.rtxtLogWindow.ScrollToCaret();
            _logger.Debug(message);
        }
    }
}
