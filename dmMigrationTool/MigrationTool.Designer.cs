﻿namespace dmMigrationTool
{
    partial class Migration_Tool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Migration_Tool));
            this.tabPages = new System.Windows.Forms.TabControl();
            this.folderPage = new System.Windows.Forms.TabPage();
            this.btnOutNameInfo = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.txtOutName = new System.Windows.Forms.TextBox();
            this.btnJavaLocation = new System.Windows.Forms.Button();
            this.btnArchiveFolder = new System.Windows.Forms.Button();
            this.btnCompFolder = new System.Windows.Forms.Button();
            this.btnDestFolder = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.txtCompressed = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblFolder1 = new System.Windows.Forms.Label();
            this.txtJava = new System.Windows.Forms.TextBox();
            this.txtArchive = new System.Windows.Forms.TextBox();
            this.txtDestination = new System.Windows.Forms.TextBox();
            this.keysPage = new System.Windows.Forms.TabPage();
            this.btnDown = new System.Windows.Forms.Button();
            this.btnUp = new System.Windows.Forms.Button();
            this.btnRemoveKey = new System.Windows.Forms.Button();
            this.btnAddKey = new System.Windows.Forms.Button();
            this.cmbValue = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbNewKey = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.lstKeys = new System.Windows.Forms.ListView();
            this.sqlPage = new System.Windows.Forms.TabPage();
            this.cmbUpdateKey = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtUpdateValue = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.queryPage = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.rtxtQueryOutput = new System.Windows.Forms.RichTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMaxRecords = new System.Windows.Forms.TextBox();
            this.txtWhere = new System.Windows.Forms.TextBox();
            this.txtTable = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.configPage = new System.Windows.Forms.TabPage();
            this.btnExportConfig = new System.Windows.Forms.Button();
            this.btnImportConfig = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.lblCounter = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnCount = new System.Windows.Forms.Button();
            this.lblAdmin = new System.Windows.Forms.Label();
            this.fbdFindFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.ofdJavaFile = new System.Windows.Forms.OpenFileDialog();
            this.sfdSaveFile = new System.Windows.Forms.SaveFileDialog();
            this.ofdConfigFile = new System.Windows.Forms.OpenFileDialog();
            this.tabPages.SuspendLayout();
            this.folderPage.SuspendLayout();
            this.keysPage.SuspendLayout();
            this.sqlPage.SuspendLayout();
            this.queryPage.SuspendLayout();
            this.configPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPages
            // 
            this.tabPages.Controls.Add(this.folderPage);
            this.tabPages.Controls.Add(this.keysPage);
            this.tabPages.Controls.Add(this.sqlPage);
            this.tabPages.Controls.Add(this.queryPage);
            this.tabPages.Controls.Add(this.configPage);
            this.tabPages.Location = new System.Drawing.Point(12, 72);
            this.tabPages.Name = "tabPages";
            this.tabPages.SelectedIndex = 0;
            this.tabPages.Size = new System.Drawing.Size(438, 195);
            this.tabPages.TabIndex = 1;
            // 
            // folderPage
            // 
            this.folderPage.BackColor = System.Drawing.Color.White;
            this.folderPage.Controls.Add(this.btnOutNameInfo);
            this.folderPage.Controls.Add(this.label17);
            this.folderPage.Controls.Add(this.txtOutName);
            this.folderPage.Controls.Add(this.btnJavaLocation);
            this.folderPage.Controls.Add(this.btnArchiveFolder);
            this.folderPage.Controls.Add(this.btnCompFolder);
            this.folderPage.Controls.Add(this.btnDestFolder);
            this.folderPage.Controls.Add(this.label13);
            this.folderPage.Controls.Add(this.txtCompressed);
            this.folderPage.Controls.Add(this.label3);
            this.folderPage.Controls.Add(this.label2);
            this.folderPage.Controls.Add(this.lblFolder1);
            this.folderPage.Controls.Add(this.txtJava);
            this.folderPage.Controls.Add(this.txtArchive);
            this.folderPage.Controls.Add(this.txtDestination);
            this.folderPage.Location = new System.Drawing.Point(4, 22);
            this.folderPage.Name = "folderPage";
            this.folderPage.Padding = new System.Windows.Forms.Padding(3);
            this.folderPage.Size = new System.Drawing.Size(430, 169);
            this.folderPage.TabIndex = 0;
            this.folderPage.Text = "Folders";
            // 
            // btnOutNameInfo
            // 
            this.btnOutNameInfo.Location = new System.Drawing.Point(409, 40);
            this.btnOutNameInfo.Name = "btnOutNameInfo";
            this.btnOutNameInfo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnOutNameInfo.Size = new System.Drawing.Size(17, 19);
            this.btnOutNameInfo.TabIndex = 14;
            this.btnOutNameInfo.Text = "i";
            this.btnOutNameInfo.UseVisualStyleBackColor = true;
            this.btnOutNameInfo.Click += new System.EventHandler(this.btnOutNameInfo_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 42);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 13);
            this.label17.TabIndex = 13;
            this.label17.Text = "Output Name";
            // 
            // txtOutName
            // 
            this.txtOutName.Location = new System.Drawing.Point(95, 39);
            this.txtOutName.Name = "txtOutName";
            this.txtOutName.Size = new System.Drawing.Size(311, 20);
            this.txtOutName.TabIndex = 12;
            // 
            // btnJavaLocation
            // 
            this.btnJavaLocation.Location = new System.Drawing.Point(409, 118);
            this.btnJavaLocation.Name = "btnJavaLocation";
            this.btnJavaLocation.Size = new System.Drawing.Size(17, 19);
            this.btnJavaLocation.TabIndex = 11;
            this.btnJavaLocation.Text = "F";
            this.btnJavaLocation.UseVisualStyleBackColor = true;
            this.btnJavaLocation.Click += new System.EventHandler(this.btnJavaLocation_Click);
            // 
            // btnArchiveFolder
            // 
            this.btnArchiveFolder.Location = new System.Drawing.Point(409, 91);
            this.btnArchiveFolder.Name = "btnArchiveFolder";
            this.btnArchiveFolder.Size = new System.Drawing.Size(17, 19);
            this.btnArchiveFolder.TabIndex = 10;
            this.btnArchiveFolder.Text = "F";
            this.btnArchiveFolder.UseVisualStyleBackColor = true;
            this.btnArchiveFolder.Click += new System.EventHandler(this.btnArchiveFolder_Click);
            // 
            // btnCompFolder
            // 
            this.btnCompFolder.Location = new System.Drawing.Point(409, 65);
            this.btnCompFolder.Name = "btnCompFolder";
            this.btnCompFolder.Size = new System.Drawing.Size(17, 19);
            this.btnCompFolder.TabIndex = 9;
            this.btnCompFolder.Text = "F";
            this.btnCompFolder.UseVisualStyleBackColor = true;
            this.btnCompFolder.Click += new System.EventHandler(this.btnCompFolder_Click);
            // 
            // btnDestFolder
            // 
            this.btnDestFolder.Location = new System.Drawing.Point(409, 14);
            this.btnDestFolder.Name = "btnDestFolder";
            this.btnDestFolder.Size = new System.Drawing.Size(17, 19);
            this.btnDestFolder.TabIndex = 8;
            this.btnDestFolder.Text = "F";
            this.btnDestFolder.UseVisualStyleBackColor = true;
            this.btnDestFolder.Click += new System.EventHandler(this.btnDestFolder_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 68);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(79, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "Temp Directory";
            // 
            // txtCompressed
            // 
            this.txtCompressed.Location = new System.Drawing.Point(95, 65);
            this.txtCompressed.Name = "txtCompressed";
            this.txtCompressed.Size = new System.Drawing.Size(311, 20);
            this.txtCompressed.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Java Executable";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Archive Folder";
            // 
            // lblFolder1
            // 
            this.lblFolder1.AutoSize = true;
            this.lblFolder1.Location = new System.Drawing.Point(6, 16);
            this.lblFolder1.Name = "lblFolder1";
            this.lblFolder1.Size = new System.Drawing.Size(84, 13);
            this.lblFolder1.TabIndex = 3;
            this.lblFolder1.Text = "Output Directory";
            // 
            // txtJava
            // 
            this.txtJava.Location = new System.Drawing.Point(95, 117);
            this.txtJava.Name = "txtJava";
            this.txtJava.Size = new System.Drawing.Size(311, 20);
            this.txtJava.TabIndex = 3;
            // 
            // txtArchive
            // 
            this.txtArchive.Location = new System.Drawing.Point(95, 91);
            this.txtArchive.Name = "txtArchive";
            this.txtArchive.Size = new System.Drawing.Size(311, 20);
            this.txtArchive.TabIndex = 2;
            // 
            // txtDestination
            // 
            this.txtDestination.Location = new System.Drawing.Point(95, 13);
            this.txtDestination.Name = "txtDestination";
            this.txtDestination.Size = new System.Drawing.Size(311, 20);
            this.txtDestination.TabIndex = 0;
            // 
            // keysPage
            // 
            this.keysPage.Controls.Add(this.btnDown);
            this.keysPage.Controls.Add(this.btnUp);
            this.keysPage.Controls.Add(this.btnRemoveKey);
            this.keysPage.Controls.Add(this.btnAddKey);
            this.keysPage.Controls.Add(this.cmbValue);
            this.keysPage.Controls.Add(this.label15);
            this.keysPage.Controls.Add(this.cmbNewKey);
            this.keysPage.Controls.Add(this.label14);
            this.keysPage.Controls.Add(this.lstKeys);
            this.keysPage.Location = new System.Drawing.Point(4, 22);
            this.keysPage.Name = "keysPage";
            this.keysPage.Padding = new System.Windows.Forms.Padding(3);
            this.keysPage.Size = new System.Drawing.Size(430, 169);
            this.keysPage.TabIndex = 3;
            this.keysPage.Text = "Keys";
            this.keysPage.UseVisualStyleBackColor = true;
            // 
            // btnDown
            // 
            this.btnDown.Location = new System.Drawing.Point(262, 128);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(23, 23);
            this.btnDown.TabIndex = 8;
            this.btnDown.Text = "v";
            this.btnDown.UseVisualStyleBackColor = true;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // btnUp
            // 
            this.btnUp.Location = new System.Drawing.Point(262, 99);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(23, 23);
            this.btnUp.TabIndex = 7;
            this.btnUp.Text = "ᴧ";
            this.btnUp.UseVisualStyleBackColor = true;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // btnRemoveKey
            // 
            this.btnRemoveKey.Location = new System.Drawing.Point(302, 128);
            this.btnRemoveKey.Name = "btnRemoveKey";
            this.btnRemoveKey.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveKey.TabIndex = 6;
            this.btnRemoveKey.Text = "Remove";
            this.btnRemoveKey.UseVisualStyleBackColor = true;
            this.btnRemoveKey.Click += new System.EventHandler(this.btnRemoveKey_Click);
            // 
            // btnAddKey
            // 
            this.btnAddKey.Location = new System.Drawing.Point(302, 99);
            this.btnAddKey.Name = "btnAddKey";
            this.btnAddKey.Size = new System.Drawing.Size(75, 23);
            this.btnAddKey.TabIndex = 5;
            this.btnAddKey.Text = "Add";
            this.btnAddKey.UseVisualStyleBackColor = true;
            this.btnAddKey.Click += new System.EventHandler(this.btnAddKey_Click);
            // 
            // cmbValue
            // 
            this.cmbValue.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbValue.FormattingEnabled = true;
            this.cmbValue.Location = new System.Drawing.Point(262, 66);
            this.cmbValue.Name = "cmbValue";
            this.cmbValue.Size = new System.Drawing.Size(162, 21);
            this.cmbValue.TabIndex = 4;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(259, 48);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 13);
            this.label15.TabIndex = 3;
            this.label15.Text = "Value";
            // 
            // cmbNewKey
            // 
            this.cmbNewKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNewKey.FormattingEnabled = true;
            this.cmbNewKey.Location = new System.Drawing.Point(262, 24);
            this.cmbNewKey.Name = "cmbNewKey";
            this.cmbNewKey.Size = new System.Drawing.Size(162, 21);
            this.cmbNewKey.TabIndex = 2;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(259, 6);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(79, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Embedded Key";
            // 
            // lstKeys
            // 
            this.lstKeys.Location = new System.Drawing.Point(6, 6);
            this.lstKeys.MultiSelect = false;
            this.lstKeys.Name = "lstKeys";
            this.lstKeys.ShowGroups = false;
            this.lstKeys.Size = new System.Drawing.Size(249, 157);
            this.lstKeys.TabIndex = 0;
            this.lstKeys.UseCompatibleStateImageBehavior = false;
            // 
            // sqlPage
            // 
            this.sqlPage.Controls.Add(this.cmbUpdateKey);
            this.sqlPage.Controls.Add(this.label16);
            this.sqlPage.Controls.Add(this.txtUpdateValue);
            this.sqlPage.Controls.Add(this.label1);
            this.sqlPage.Controls.Add(this.label7);
            this.sqlPage.Controls.Add(this.txtPassword);
            this.sqlPage.Controls.Add(this.label4);
            this.sqlPage.Controls.Add(this.label5);
            this.sqlPage.Controls.Add(this.label6);
            this.sqlPage.Controls.Add(this.txtUsername);
            this.sqlPage.Controls.Add(this.txtDatabase);
            this.sqlPage.Controls.Add(this.txtServer);
            this.sqlPage.Location = new System.Drawing.Point(4, 22);
            this.sqlPage.Name = "sqlPage";
            this.sqlPage.Padding = new System.Windows.Forms.Padding(3);
            this.sqlPage.Size = new System.Drawing.Size(430, 169);
            this.sqlPage.TabIndex = 1;
            this.sqlPage.Text = "SQL";
            this.sqlPage.UseVisualStyleBackColor = true;
            // 
            // cmbUpdateKey
            // 
            this.cmbUpdateKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUpdateKey.FormattingEnabled = true;
            this.cmbUpdateKey.Location = new System.Drawing.Point(79, 117);
            this.cmbUpdateKey.Name = "cmbUpdateKey";
            this.cmbUpdateKey.Size = new System.Drawing.Size(79, 21);
            this.cmbUpdateKey.TabIndex = 18;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(164, 121);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(34, 13);
            this.label16.TabIndex = 17;
            this.label16.Text = "Value";
            // 
            // txtUpdateValue
            // 
            this.txtUpdateValue.Location = new System.Drawing.Point(204, 118);
            this.txtUpdateValue.Name = "txtUpdateValue";
            this.txtUpdateValue.Size = new System.Drawing.Size(208, 20);
            this.txtUpdateValue.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Update Key";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 95);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Password";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(79, 92);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(333, 20);
            this.txtPassword.TabIndex = 3;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Username";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Database";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Server Name";
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(79, 65);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(333, 20);
            this.txtUsername.TabIndex = 2;
            // 
            // txtDatabase
            // 
            this.txtDatabase.Location = new System.Drawing.Point(79, 39);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(333, 20);
            this.txtDatabase.TabIndex = 1;
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(79, 13);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(333, 20);
            this.txtServer.TabIndex = 0;
            // 
            // queryPage
            // 
            this.queryPage.Controls.Add(this.label11);
            this.queryPage.Controls.Add(this.rtxtQueryOutput);
            this.queryPage.Controls.Add(this.label8);
            this.queryPage.Controls.Add(this.label10);
            this.queryPage.Controls.Add(this.txtMaxRecords);
            this.queryPage.Controls.Add(this.txtWhere);
            this.queryPage.Controls.Add(this.txtTable);
            this.queryPage.Controls.Add(this.label9);
            this.queryPage.Location = new System.Drawing.Point(4, 22);
            this.queryPage.Name = "queryPage";
            this.queryPage.Padding = new System.Windows.Forms.Padding(3);
            this.queryPage.Size = new System.Drawing.Size(430, 169);
            this.queryPage.TabIndex = 2;
            this.queryPage.Text = "Query";
            this.queryPage.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 91);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Query";
            // 
            // rtxtQueryOutput
            // 
            this.rtxtQueryOutput.Enabled = false;
            this.rtxtQueryOutput.Location = new System.Drawing.Point(9, 107);
            this.rtxtQueryOutput.Name = "rtxtQueryOutput";
            this.rtxtQueryOutput.Size = new System.Drawing.Size(406, 56);
            this.rtxtQueryOutput.TabIndex = 20;
            this.rtxtQueryOutput.Text = "";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 68);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "WHERE";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Max Records";
            // 
            // txtMaxRecords
            // 
            this.txtMaxRecords.Location = new System.Drawing.Point(82, 13);
            this.txtMaxRecords.Name = "txtMaxRecords";
            this.txtMaxRecords.Size = new System.Drawing.Size(333, 20);
            this.txtMaxRecords.TabIndex = 0;
            this.txtMaxRecords.TextChanged += new System.EventHandler(this.txtMaxRecords_TextChanged);
            // 
            // txtWhere
            // 
            this.txtWhere.Location = new System.Drawing.Point(82, 65);
            this.txtWhere.Name = "txtWhere";
            this.txtWhere.Size = new System.Drawing.Size(333, 20);
            this.txtWhere.TabIndex = 2;
            this.txtWhere.TextChanged += new System.EventHandler(this.txtWhere_TextChanged);
            // 
            // txtTable
            // 
            this.txtTable.Location = new System.Drawing.Point(82, 39);
            this.txtTable.Name = "txtTable";
            this.txtTable.Size = new System.Drawing.Size(333, 20);
            this.txtTable.TabIndex = 1;
            this.txtTable.TextChanged += new System.EventHandler(this.txtTable_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 42);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Table";
            // 
            // configPage
            // 
            this.configPage.Controls.Add(this.btnExportConfig);
            this.configPage.Controls.Add(this.btnImportConfig);
            this.configPage.Location = new System.Drawing.Point(4, 22);
            this.configPage.Name = "configPage";
            this.configPage.Padding = new System.Windows.Forms.Padding(3);
            this.configPage.Size = new System.Drawing.Size(430, 169);
            this.configPage.TabIndex = 4;
            this.configPage.Text = "Config";
            this.configPage.UseVisualStyleBackColor = true;
            // 
            // btnExportConfig
            // 
            this.btnExportConfig.Location = new System.Drawing.Point(219, 59);
            this.btnExportConfig.Name = "btnExportConfig";
            this.btnExportConfig.Size = new System.Drawing.Size(87, 41);
            this.btnExportConfig.TabIndex = 1;
            this.btnExportConfig.Text = "Export Config";
            this.btnExportConfig.UseVisualStyleBackColor = true;
            this.btnExportConfig.Click += new System.EventHandler(this.btnExportConfig_Click);
            // 
            // btnImportConfig
            // 
            this.btnImportConfig.Location = new System.Drawing.Point(126, 59);
            this.btnImportConfig.Name = "btnImportConfig";
            this.btnImportConfig.Size = new System.Drawing.Size(87, 41);
            this.btnImportConfig.TabIndex = 0;
            this.btnImportConfig.Text = "Import Config";
            this.btnImportConfig.UseVisualStyleBackColor = true;
            this.btnImportConfig.Click += new System.EventHandler(this.btnImportConfig_Click);
            // 
            // btnRun
            // 
            this.btnRun.Enabled = false;
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(235, 273);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(118, 39);
            this.btnRun.TabIndex = 0;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // lblCounter
            // 
            this.lblCounter.AutoSize = true;
            this.lblCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCounter.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblCounter.Location = new System.Drawing.Point(12, 45);
            this.lblCounter.Name = "lblCounter";
            this.lblCounter.Size = new System.Drawing.Size(127, 24);
            this.lblCounter.TabIndex = 3;
            this.lblCounter.Text = "Document 0/0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label12.Location = new System.Drawing.Point(5, 8);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(296, 37);
            this.label12.TabIndex = 4;
            this.label12.Text = "DM Migration Tool";
            // 
            // btnCount
            // 
            this.btnCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCount.Location = new System.Drawing.Point(111, 273);
            this.btnCount.Name = "btnCount";
            this.btnCount.Size = new System.Drawing.Size(118, 39);
            this.btnCount.TabIndex = 5;
            this.btnCount.Text = "Get Count";
            this.btnCount.UseVisualStyleBackColor = true;
            this.btnCount.Click += new System.EventHandler(this.btnCount_Click);
            // 
            // lblAdmin
            // 
            this.lblAdmin.AutoSize = true;
            this.lblAdmin.Location = new System.Drawing.Point(431, -1);
            this.lblAdmin.Name = "lblAdmin";
            this.lblAdmin.Size = new System.Drawing.Size(36, 13);
            this.lblAdmin.TabIndex = 6;
            this.lblAdmin.Text = "Admin";
            this.lblAdmin.Visible = false;
            // 
            // ofdJavaFile
            // 
            this.ofdJavaFile.FileName = "openFileDialog1";
            this.ofdJavaFile.Filter = "Java|java.exe";
            // 
            // sfdSaveFile
            // 
            this.sfdSaveFile.DefaultExt = "xml";
            this.sfdSaveFile.FileName = "MigrationToolConfig.xml";
            this.sfdSaveFile.Filter = "XML|*.xml";
            // 
            // ofdConfigFile
            // 
            this.ofdConfigFile.FileName = "openFileDialog1";
            this.ofdConfigFile.Filter = "XML|*.xml";
            // 
            // Migration_Tool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(464, 325);
            this.Controls.Add(this.lblAdmin);
            this.Controls.Add(this.btnCount);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lblCounter);
            this.Controls.Add(this.btnRun);
            this.Controls.Add(this.tabPages);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Migration_Tool";
            this.Text = "Migration Tool";
            this.tabPages.ResumeLayout(false);
            this.folderPage.ResumeLayout(false);
            this.folderPage.PerformLayout();
            this.keysPage.ResumeLayout(false);
            this.keysPage.PerformLayout();
            this.sqlPage.ResumeLayout(false);
            this.sqlPage.PerformLayout();
            this.queryPage.ResumeLayout(false);
            this.queryPage.PerformLayout();
            this.configPage.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabPages;
        private System.Windows.Forms.TabPage folderPage;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblFolder1;
        private System.Windows.Forms.TabPage sqlPage;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblCounter;
        private System.Windows.Forms.TabPage queryPage;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RichTextBox rtxtQueryOutput;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TabPage keysPage;
        private System.Windows.Forms.Button btnRemoveKey;
        private System.Windows.Forms.Button btnAddKey;
        private System.Windows.Forms.ComboBox cmbValue;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmbNewKey;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Button btnCount;
        private System.Windows.Forms.Label lblAdmin;
        private System.Windows.Forms.Button btnDestFolder;
        private System.Windows.Forms.FolderBrowserDialog fbdFindFolder;
        private System.Windows.Forms.Button btnCompFolder;
        private System.Windows.Forms.Button btnArchiveFolder;
        private System.Windows.Forms.Button btnJavaLocation;
        private System.Windows.Forms.OpenFileDialog ofdJavaFile;
        private System.Windows.Forms.TabPage configPage;
        private System.Windows.Forms.Button btnExportConfig;
        private System.Windows.Forms.Button btnImportConfig;
        private System.Windows.Forms.SaveFileDialog sfdSaveFile;
        public System.Windows.Forms.TextBox txtJava;
        public System.Windows.Forms.TextBox txtArchive;
        public System.Windows.Forms.TextBox txtDestination;
        public System.Windows.Forms.TextBox txtCompressed;
        public System.Windows.Forms.TextBox txtPassword;
        public System.Windows.Forms.TextBox txtUsername;
        public System.Windows.Forms.TextBox txtDatabase;
        public System.Windows.Forms.TextBox txtServer;
        public System.Windows.Forms.TextBox txtWhere;
        public System.Windows.Forms.TextBox txtTable;
        public System.Windows.Forms.TextBox txtMaxRecords;
        public System.Windows.Forms.ListView lstKeys;
        private System.Windows.Forms.OpenFileDialog ofdConfigFile;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.TextBox txtUpdateValue;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox cmbUpdateKey;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox txtOutName;
        private System.Windows.Forms.Button btnOutNameInfo;
    }
}

