﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dmMigrationTool
{
    class StatusTracker
    {
        private int successful;
        private List<string> problemDocIds;

        public StatusTracker()
        {
            problemDocIds = new List<string>();
            successful = 0;
        }

        public void ProcessedDoc()
        {
            successful++;
        }

        public void ProcessedDoc(string docId)
        {
            problemDocIds.Add(docId);
        }

        public string GenerateReport()
        {
            var problemDocs = problemDocIds.Count();
            var report = new StringBuilder();
            
            report.AppendLine("REPORT START");
            report.AppendLine((problemDocs + successful) + " documents processed");
            report.AppendLine(successful + " were successful");
            report.AppendLine(problemDocs + " were unsuccessful");
            report.AppendLine("REPORT END");

            successful = 0;
            problemDocIds.Clear();

            return report.ToString();
        }
    }
}
