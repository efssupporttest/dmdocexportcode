﻿namespace dmMigrationTool
{
    partial class LoggingWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoggingWindow));
            this.rtxtLogWindow = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // rtxtLogWindow
            // 
            this.rtxtLogWindow.Location = new System.Drawing.Point(12, 12);
            this.rtxtLogWindow.Name = "rtxtLogWindow";
            this.rtxtLogWindow.Size = new System.Drawing.Size(580, 513);
            this.rtxtLogWindow.TabIndex = 0;
            this.rtxtLogWindow.Text = "";
            // 
            // LoggingWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(240)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(604, 537);
            this.Controls.Add(this.rtxtLogWindow);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LoggingWindow";
            this.Text = "DM Migration Log";
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.RichTextBox rtxtLogWindow;

    }
}