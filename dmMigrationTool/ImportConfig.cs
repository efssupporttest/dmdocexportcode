﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace dmMigrationTool
{
    class ImportConfig
    {
        private readonly Migration_Tool migrationForm;
        private readonly string saveFile;

        public ImportConfig(Migration_Tool migrationTool, string filename)
        {
            migrationForm = migrationTool;
            saveFile = filename;
        }

        public void ImportValues()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(File.ReadAllText(saveFile));
            
            /*Importing folders settings*/
            migrationForm.txtDestination.Text = xmlDoc.SelectSingleNode("/DM-Migrataion-Config/FoldersTab/Destination").InnerText;
            migrationForm.txtOutName.Text = xmlDoc.SelectSingleNode("/DM-Migrataion-Config/FoldersTab/OutFileName").InnerText;
            migrationForm.txtCompressed.Text = xmlDoc.SelectSingleNode("/DM-Migrataion-Config/FoldersTab/Compressed").InnerText;
            migrationForm.txtArchive.Text = xmlDoc.SelectSingleNode("/DM-Migrataion-Config/FoldersTab/Archive").InnerText;
            migrationForm.txtJava.Text = xmlDoc.SelectSingleNode("/DM-Migrataion-Config/FoldersTab/Java").InnerText;

            /*Importing keys and values*/
            XmlNodeList keyNodeList = xmlDoc.SelectNodes("/DM-Migrataion-Config/KeysTab/Key");
            if(keyNodeList.Count != 0)
            {
                foreach(XmlNode key in keyNodeList)
                {
                    string[] newKey = new string[2] { key["KeyName"].InnerText, key["KeyValue"].InnerText };
                    migrationForm.lstKeys.Items.Add(new ListViewItem(newKey));
                }
            }

            /*Importing SQL settings*/
            migrationForm.txtServer.Text = xmlDoc.SelectSingleNode("/DM-Migrataion-Config/SQLTab/Server-Name").InnerText;
            migrationForm.txtDatabase.Text = xmlDoc.SelectSingleNode("/DM-Migrataion-Config/SQLTab/Database").InnerText;
            migrationForm.txtUsername.Text = xmlDoc.SelectSingleNode("/DM-Migrataion-Config/SQLTab/Username").InnerText;
            byte[] passwordBytes = Convert.FromBase64String(xmlDoc.SelectSingleNode("/DM-Migrataion-Config/SQLTab/Password").InnerText);
            migrationForm.txtPassword.Text = Encoding.UTF8.GetString(passwordBytes);
            migrationForm.cmbUpdateKey.SelectedIndex = Convert.ToInt16(xmlDoc.SelectSingleNode("/DM-Migrataion-Config/SQLTab/Update-Key").InnerText);
            migrationForm.txtUpdateValue.Text = xmlDoc.SelectSingleNode("/DM-Migrataion-Config/SQLTab/Update-Value").InnerText;

            /*Importing Query settings*/
            migrationForm.txtMaxRecords.Text = xmlDoc.SelectSingleNode("/DM-Migrataion-Config/QueryTab/Max-Records").InnerText;
            migrationForm.txtTable.Text = xmlDoc.SelectSingleNode("/DM-Migrataion-Config/QueryTab/Table").InnerText;
            migrationForm.txtWhere.Text = xmlDoc.SelectSingleNode("/DM-Migrataion-Config/QueryTab/Where").InnerText;
        }
    }
}
