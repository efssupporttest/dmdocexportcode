﻿using System;
using System.Windows.Forms;


namespace dmMigrationTool
{
    public partial class LoggingWindow : Form
    {
        public LoggingWindow()
        {
            InitializeComponent();
            rtxtLogWindow.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
            rtxtLogWindow.AppendText("[" + DateTime.Now.ToString("HH:mm:ss dd-MM-yy") + "] START -  Logging started");
            rtxtLogWindow.ReadOnly = true;
        }
    }
}
