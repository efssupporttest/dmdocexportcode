﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace dmMigrationTool
{
    public partial class FileNameInformation : Form
    {
        private ListBox lstDatabaseKeys = new ListBox();

        public FileNameInformation()
        {
            InitializeComponent();
        }

        private void FileNameInformation_Load(object sender, EventArgs e)
        {
            StringBuilder informationString = new StringBuilder();
            informationString.AppendLine("blank - Will used the docID as default");
            informationString.AppendLine("text - will use plain text");
            informationString.AppendLine("#i# - Will give an incremental number if filename already exists (will max out at 10)");
            informationString.AppendLine("#column# - Will subsitute for a SQL column value");
            lblInfo.Text = informationString.ToString();

            var keyList = new KeyList();
            List<string> databaseKeys = keyList.GetDatabaseMap();
            foreach(var key in databaseKeys)
            {
                lstDatabaseKeys.Items.Add(key);
            }
            lstDatabaseKeys.Size = new System.Drawing.Size(100, 100);
            lstDatabaseKeys.Location = new System.Drawing.Point(10, 125);

            this.Controls.Add(lstDatabaseKeys);
            this.Refresh();
        }
    }
}
