﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace dmMigrationTool
{
    class Validation
    {
        private bool setDFMT { get; set; }
        private string pdmDFMT { get; set; }
        
        public bool CheckKey(string key, string data)
        {
            KeyList keyList = new KeyList("temp");
            if (key.Contains("NKEY") && !(keyList.sqlFields.Contains(data) || Regex.IsMatch(data, @"^\d+$")))
            {
                MessageBox.Show("An NKEY must be a number.");
                return false;
            }
            else if (key.Contains("DKEY") && !keyList.sqlFields.Contains(data))
            {
                DateTime outDate;
                if (!setDFMT)
                {
                    MessageBox.Show("PDMDFMT must be set before any DKEYs");
                    return false;
                }
                else if (!DateTime.TryParseExact(data, pdmDFMT, null, DateTimeStyles.None, out outDate))
                {
                    MessageBox.Show("The DKEY must be in the same format as the PDMDFMT");
                    return false;
                }
            }
            return true;
        }

        public void SetDFMT(bool set, string value)
        {
            setDFMT = set;
            pdmDFMT = value;
        }

        public bool Folder(string folder, bool java)
        {
            string regexMask = @"^(\w:|\\{2}\w+\\\w+(\$|))\\.*\\";
            if (java)
                regexMask += "(j|J)ava.exe";
            
            regexMask += "$"; //in regex this sybolises the end of a string
            if (Regex.IsMatch(folder, regexMask))
                return true;
            return false;
        }
    }
}
