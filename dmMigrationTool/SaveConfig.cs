﻿using System.Windows.Forms;
using System.Xml;
using System.Text;
using System;

namespace dmMigrationTool
{
    class SaveConfig
    {
        private static Migration_Tool migrationForm;
        private static string saveFile;

        public SaveConfig(Migration_Tool migrationTool, string filename)
        {
            migrationForm = migrationTool;
            saveFile = filename;
        }
        
        public void SaveValues()
        {
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.NewLineOnAttributes = true;
            xmlWriterSettings.Indent = true;

            using (XmlWriter xmlWriter = XmlWriter.Create(saveFile, xmlWriterSettings))
            {
                xmlWriter.WriteStartDocument();
                xmlWriter.WriteStartElement("DM-Migrataion-Config");

                xmlWriter.WriteStartElement("FoldersTab");
                xmlWriter.WriteElementString("Destination", migrationForm.txtDestination.Text);
                xmlWriter.WriteElementString("OutFileName", migrationForm.txtOutName.Text);
                xmlWriter.WriteElementString("Compressed", migrationForm.txtCompressed.Text);
                xmlWriter.WriteElementString("Archive", migrationForm.txtArchive.Text);
                xmlWriter.WriteElementString("Java", migrationForm.txtJava.Text);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("KeysTab");
                foreach (ListViewItem item in migrationForm.lstKeys.Items)
                {
                    xmlWriter.WriteStartElement("Key");
                    xmlWriter.WriteElementString("KeyName", item.SubItems[0].Text);
                    xmlWriter.WriteElementString("KeyValue", item.SubItems[1].Text);
                    xmlWriter.WriteEndElement();
                }
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("SQLTab");
                xmlWriter.WriteElementString("Server-Name", migrationForm.txtServer.Text);
                xmlWriter.WriteElementString("Database", migrationForm.txtDatabase.Text);
                xmlWriter.WriteElementString("Username", migrationForm.txtUsername.Text);
                string password = migrationForm.txtPassword.Text;
                password = Convert.ToBase64String(Encoding.UTF8.GetBytes(password));
                xmlWriter.WriteElementString("Password", password);
                xmlWriter.WriteElementString("Update-Key", migrationForm.cmbUpdateKey.SelectedIndex.ToString());
                xmlWriter.WriteElementString("Update-Value", migrationForm.txtUpdateValue.Text);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("QueryTab");
                xmlWriter.WriteElementString("Max-Records", migrationForm.txtMaxRecords.Text);
                xmlWriter.WriteElementString("Table", migrationForm.txtTable.Text);
                xmlWriter.WriteElementString("Where", migrationForm.txtWhere.Text);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndDocument();
            }
        }
    }
}
