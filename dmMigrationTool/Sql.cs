﻿namespace dmMigrationTool
{
    class Sql
    {
        public string dbUrl { get; set; }
        public string dbDatabase { get; set; }
        public string dbUser { get; set; }
        public string dbPassword { get; set; }

        public void SetDb(string url, string db, string user, string pass)
        {
            dbUrl = url;
            dbDatabase = db;
            dbUser = user;
            dbPassword = pass;
        }

        public string CreateSelectQuery(bool count, string max, string table, string where)
        {
            string query = "SELECT";
            if (count)
                query += " COUNT(*)";
            else if (max != "")
                query += " TOP " + max + " * ";
            else
                query += " *";
            query += " FROM " + table + " WHERE " + where;
            return query;
        }
    }
}
