﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dmMigrationTool
{
    class KeyList
    {
        public List<string> sqlFields { get; set; }
        public List<string> pdmKeys { get; set; }
        public List<string> updateKeys { get; set; }

        public KeyList(string noUpdate)
        {
            sqlFields = AddDatabaseKeys();
            pdmKeys = new List<string>();
            AddPDMKeys();
            updateKeys = new List<string>();
            AddUpdateKeys(noUpdate);
            
        }

        public KeyList()
        {   }

        public List<string> GetDatabaseMap()
        {
            var databaseMap = AddDatabaseKeys();
            return databaseMap;
        }

        private void AddUpdateKeys(string noUpdate)
        {
            updateKeys.Add(noUpdate);
            updateKeys = LoopKeys(updateKeys, "PDMSKEY", 25);
            updateKeys = LoopKeys(updateKeys, "PDMNKEY", 15);
            updateKeys = LoopKeys(updateKeys, "PDMDKEY", 15);
        }

        private void AddPDMKeys()
        {
            pdmKeys.Add("PDMCUK");
            pdmKeys.Add("PDMDOC");
            pdmKeys.Add("PDMDFMT");
            //pdmKeys.Add("PDMATT");    //is currently being set programatically
            pdmKeys.Add("PDMUDP");
            pdmKeys = LoopKeys(pdmKeys, "PDMSKEY", 25);
            pdmKeys = LoopKeys(pdmKeys, "PDMNKEY", 15);
            pdmKeys = LoopKeys(pdmKeys, "PDMDKEY", 15);
        }

        private List<string> LoopKeys(List<string> tempList, string key, int max)
        {
            for( int i = 1; i <= max; i++)
            {
                tempList.Add(key + i);
            }
            return tempList;
        }

        private List<string> AddDatabaseKeys()
        {
            var tempList = new List<string>();
            tempList.Add("docID");
            tempList.Add("docName");
            tempList.Add("revision");
            tempList.Add("fileVersion");
            tempList.Add("docTypeID");
            tempList.Add("ownerID");
            tempList.Add("lastChanged");
            tempList.Add("fileArchived");
            tempList.Add("isDeleted");
            tempList.Add("fileRootID");
            tempList.Add("fileSizeUncompressed");
            tempList.Add("fileSizeCompressed");
            tempList.Add("fileMD5Hash");
            tempList.Add("fileIsCompressed");
            tempList.Add("fileType");
            tempList.Add("noteDefID");
            tempList.Add("noteId");
            tempList = LoopKeys(tempList, "PDMSKEY", 25);
            tempList = LoopKeys(tempList, "PDMNKEY", 15);
            tempList = LoopKeys(tempList, "PDMDKEY", 15);
            tempList.Add("customerUniqueID");
            tempList.Add("lastLocation");
            return tempList;
        }

    }
}
